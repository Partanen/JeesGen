#include <assert.h>
#include "utils.c"
#include "ksys.h"

#define CFG_FILE_NAME_LEN   15
#define MAX_PL_TOKENS       3
#define MAX_PAGE_TOKENS     3
#define MAX_PAGE_NAME_LEN   64

typedef struct post_t               post_t;
typedef struct cache_t              cache_t;
typedef struct token_t              token_t;
typedef struct dt_unit_t            dt_unit_t;
typedef struct page_t               page_t;
typedef struct page_link_layout_t   page_link_layout_t;
typedef page_t                      page_darr_t;
typedef post_t                      post_darr_t;

enum date_unit_types {
    DU_DAY = 1,
    DU_MONTH,
    DU_YEAR
};

enum time_format_types {
    TF_SHOW_HOUR_MIN_SEC    = 0,
    TF_SHOW_HOUR_MIN        = 1,
    TF_SHOW_HOUR            = 2
};

enum post_token_types {
    POST_TOK_TITLE = 0,
    POST_TOK_AUTHOR,
    POST_TOK_DATE,
    POST_TOK_TIME,
    POST_TOK_CONTENT,
    POST_TOK_STRING_ID,
    POST_TOK_HTML,
    POST_TOK_POST_URL,
    MAX_POST_TOKS
};

enum post_list_token_types {
    PL_TOK_POSTS,
    PL_TOK_POST_PAGE_LINKS,
    PL_TOK_HTML
};

enum page_token_types {
    PAGE_TOK_CONTENT = 0,
    PAGE_TOK_PAGE_LINKS,
    PAGE_TOK_HTML
};

enum link_token_types {
    LINK_TOK_PATH,
    LINK_TOK_NAME,
    LINK_TOK_HTML
};

enum individual_post_token_types {
    IND_POST_TOK_CONTENT,
    IND_POST_TOK_BACK_URL,
    IND_POST_TOK_HTML,
    MAX_IND_POST_TOKS
};

struct post_t
{
    uint64      id;
    dchar       *title;
    dchar       *author;
    dchar       *path;
    uint        date[3];
    uint8       time[3];
};

struct page_t {
    dchar   *path;
    dchar   *name;
    int     priority;
    bool8   create_menu_entry;
};

struct cache_t {
    uint64 running_post_id;
};

struct token_t {
    char    *head;       /* What to print first */
    char    *tag_offset;
    int     type;
};

struct dt_unit_t {
    int8    type;
    bool8   numeric;
    int     max_chars;
};

struct page_link_layout_t {
    token_t toks[3];
    int     num_toks;
    dchar   *dstr;
};

static struct {
    int             posts_per_page;
    dt_unit_t       date_format[3];
    int             time_format;
    int             post_page_links_per_page;
    int             post_page_priority;
    uint            max_rss_desc_len;
    uint            max_rss_items;
    bool32          generate_rss;
    bool32          use_post_titles_as_ids;
    bool32          generate_individual_post_pages;
    dchar           *posts_page_name;
    dchar           *site_link;
    dchar           *site_title;
    dchar           *site_desc;
    dchar           *site_post_dir_addr;
} _cfg;

static char *_tag_str_posts             = "</jees-posts>";
static char *_tag_str_title             = "</jees-title>";
static char *_tag_str_author            = "</jees-author>";
static char *_tag_str_date              = "</jees-date>";
static char *_tag_str_time              = "</jees-time>";
static char *_tag_str_content           = "</jees-content>";
static char *_tag_str_post_page_links   = "</jees-post_page_links>";
static char *_tag_str_page_links        = "</jees-page_links>";
static char *_tag_str_link_path         = "</jees-link_path>";
static char *_tag_str_link_name         = "</jees-page_name>";
static char *_tag_str_post_string_id    = "</jees-post_string_id>";
static char *_tag_str_post_url          = "</jees-post_url>";
static char *_tag_str_back_url          = "</jees-back_url>";

static char *_empty_str             = "";
static char *_nomem_str             = "out of memory";
static char *_not_enough_parms_str  = "not enough parameters";
static char *_too_many_parms_str    = "too many parameters";
static char *_unknown_str           = "unknown";
static char *_default_cfg_str       =
    "posts per page                 = 10\n"
    "date format                    = DDMMYYYY\n"
    "time format                    = HMS\n"
    "page links per page            = 10\n"
    "post page priority             = 0\n"
    "posts page name                = news\n"
    "use post titles as ids         = true\n"
    "max rss items                  = 10\n"
    "max rss description length     = 128\n"
    "generate rss                   = true\n"
    "generate individual post pages = true\n"
    "site link                      =\n"
    "site title                     =\n"
    "site description               =\n"
    "site post directory address    =\n";

static char *_cmd_strs_help[2]      = {"-h", "--help"};
static char *_cmd_strs_new[2]       = {"-n", "--new"};
static char *_cmd_strs_generate[2]  = {"-g", "--generate"};
static char *_cmd_strs_post[2]      = {"-p", "--post"};
static char *_cmd_strs_page[2]      = {"-a", "--page"};

static int
_help();

static int
_generate(int argc, char **argv);

static int
_create_page_darr(const char *proj_path, page_darr_t **ret_pages);

static void
_read_config_cb(void *ctx, const char *opt, const char *val);

static void
_read_config(const char *dir_path);

static void
_panic(int code);

static int
_fwrite_post_content(FILE *f, post_t *p, token_t *tokens, uint num_tokens);

static int
_fwrite_individual_post_page_content(FILE *f, post_t *p, uint post_index,
    uint num_posts, token_t *post_toks, uint num_post_toks,
    token_t *ind_post_toks, uint num_ind_post_toks);

static int
_generate_post_pages(const char *proj_path, const char *opt_out_dir_path,
    token_t *page_toks, uint num_page_toks,
    token_t *post_list_toks, uint num_post_list_toks, post_darr_t *posts,
    page_darr_t *pages, page_link_layout_t *link_lo,
    page_link_layout_t *cur_link_lo);

static int
_generate_pages(const char *proj_path, const char *opt_out_dir_path,
    page_darr_t *pages, token_t *page_toks, uint num_page_toks,
    page_link_layout_t *link_lo, page_link_layout_t *cur_link_lo);

static int
_read_str_until_new_line_or_null(char *str, char *buf, int buf_len);

static int
_null_terminate_str_at_first_new_line(char *s);
/* Returns offset of the termination, even if there's no new line. */

static int
_post(const char *proj_dp, const char *post_fp);

static int
_new(const char *fp);

static void
_read_cache_cb(void *ctx, const char *opt, const char *val);

static int
_read_cache(cache_t *c, const char *path);

static int
_save_cache(cache_t *cache, const char *fp);

static int
_compare_tokens(const void *a, const void *b);

static int
_compare_posts(const void *a, const void *b);

static int
_compare_pages(const void *a, const void *b);

static int
_read_page_layout(const char *fp, dchar **ret_lo,
    token_t ret_toks[MAX_PAGE_TOKENS]);
/* Returns < 0 on error, otherwise number of tokens read */

static void
_sort_and_terminate_tokens(char *str, token_t *toks, int num);

static int
_new_page(int argc, char **argv);

static int
_fwrite_page_links(FILE *f, page_darr_t *pages, page_t *page,
    page_link_layout_t *link_lo, page_link_layout_t *cur_link_lo);

static int
_page_link_layout_load(page_link_layout_t *lo, const char *fp);

static int
_generate_rss(post_darr_t *posts, const char *fp);
/* This assumes that the posts have already been sorted lowest id to highest */

int main(int argc, char **argv)
{
    for (int i = 1; i < argc; ++i) {
        #define CHECK_CMD(str_arr) \
            (streq(argv[i], (str_arr)[0]) || streq(argv[i], (str_arr)[1]))

        if (CHECK_CMD(_cmd_strs_help))
            return _help(argc - 2, argv + 2);
        else if (CHECK_CMD(_cmd_strs_generate)) {
            if (argc >= i + 2)
                return _generate(argc - i - 1, &argv[i + 1]);
        } else if (CHECK_CMD(_cmd_strs_post))
            {if (argc == i + 3) return _post(argv[i + 1], argv[i + 2]);}
        else if (CHECK_CMD(_cmd_strs_new))
            {if (argc == i + 2) return _new(argv[i + 1]);}
        else if (CHECK_CMD(_cmd_strs_page))
            return _new_page(argc - i - 1, &argv[i + 1]);
        else {
            printf("Invalid argument: %s\n", argv[i]);
            return 1;
        }
        #undef CHECK_CMD
    }
    puts("jeesgen: not enough parameters.\nSee 'jeesgen --help'");
    return 2;
}

static int
_help(int argc, char **argv)
{
    if (!argc) {
        puts(
"jeesgen static website generator\n"
"Usage: jeesgen [OPTS] [ARGS]\n\n"
"-h, --help [(OPTIONAL) OPTION]:\n"
"                               Print this dialogue. If OPTION is specified,\n"
"                               help is given for the command line option in\n"
"                               question.\n"
"-n, --new [PROJECT_PATH]:      Create a new project.\n"
"-g, --generate [(OPTIONAL) -o, --output [OUTPUT_DIR]] [PROJECT_DIR]:\n"
"                               Generate HTML pages for the given project.\n"
"-p, --post [PROJECT_DIR] [POST_FILE]:\n"
"                               Add a new post to the given project from the\n"
"                               given file. Generates timestamps from the\n"
"                               current date.\n"
"-a, --page [PORJECT_DIR] [PAGE_FILE]:\n"
"                               Add a new page to a project or update an\n"
"                               existing one.\n");
    return 0;
    }

    #define CHECK_CMD(strs) \
        (streq(argv[0], strs[0]) || streq(argv[0], strs[1]) || \
         streq(argv[0], strs[0] + 1) || streq(argv[0], strs[1] + 2))

    if (CHECK_CMD(_cmd_strs_help))
        puts("-h, --help [(OPTIONAL) OPTION]:\n"
"    If OPTION is specified print help for that program feature. Otherwise\n"
"    print default program help.\nExample 1: jeesgen -h post\n"
"Example 2: jeesgen -h -p");
    else if (CHECK_CMD(_cmd_strs_new)) {
        puts("-n, --new [PROJECT_PATH]:\n"
"    Create an empty project to PROJECT_PATH. The following files may then be\n"
"    modified or replaced to customize the project:\n"
"    * jees_config.cfg\n    * post_layout.html\n    * post_list_layout.html\n"
"    * page_layout.html\n    * page_link_layout.html\n"
"    * current_page_link_layout.html\n");
    } else if (CHECK_CMD(_cmd_strs_generate)) {
        puts(
"-g, --generate [(OPTIONAL) -o, --output [OUTPUT_DIR]] [PROJECT_DIR]:\n"
"    Generate static pages from posts contained within a project.\n"
"    --output [OUTPUT_DIR] may be used to specify an output directory for the\n"
"    generated files. If this option is not specified, a directory called\n"
"    \"generated\" will be used inside the project.\n"
"    PROJECT_DIR is the path to the project.\n"
"    Example 1: jeesgen --generate my_project\n"
"    Example 2: jeesgen --generate --output my_output_dir my_project");
    } else if (CHECK_CMD(_cmd_strs_post)) {
        puts("-p, --post [PROJECT_DIR] [POST_FILE]:\n"
"    Create a new post from a text file. PROJECT_DIR is the path to the\n"
"    project directory, POST_FILE is the path to the text file to be posted.\n"
"    The post may contain HTML/CSS and must adhere to the following format:\n"
"    ---------------------\n    title = my post title\n    author = me myself\n"
"    ...content...\n    ---------------------"

);
    } else if (CHECK_CMD(_cmd_strs_page)) {
        puts(
"-a, --page [PORJECT_DIR] [PAGE_FILE]:\n"
"    Add a new page to a project or update an existing one.\n"
"    PROJECT_DIR is the path to the project.\n"
"    PAGE_FILE is the path to a text file that will be the content of the page."
"\n    The file may contain HTML/CSS and must adhere to the following format:\n"
"    ---------------------\n"
"    name = my page\n"
"    priority = 2\n"
"    create menu entry = 1\n"
"    ...content...\n"
"    ---------------------\n"
"    The options at the top are configuration options. The 'name' field is\n"
"    mandatory, 'priority' and 'create menu entry' are optional.\n"
"    'priority' determines the ordering of the page in the list of menu\n"
"    entries. 'create menu entry' determines whether or not an entry is made\n"
"    for this page in the page link list.\n");
    } else {
        puts("Invalid argument - see --help for available options.");
        return 2;
    }
    #undef CHECK_CMD
    return 0;
}

static int
_generate(int argc, char **argv)
{
    char                *proj_path              = 0;
    char                *opt_out_dir_path       = 0;
    char                *err_str                = _unknown_str;
    dchar               *gen_dir_path           = 0;
    dchar               *post_list_layout_str   = 0;
    dchar               *page_layout_str        = 0;
    dchar               *posts_dir_path         = 0;
    int                 ret                     = 0;
    dir_handle_t        dir                     = 0;
    post_darr_t         *posts                  = 0;
    int                 num_post_list_toks      = 0;
    int                 num_page_toks           = 0;
    dchar               *dstr                   = 0;
    page_darr_t         *pages                  = 0;
    FILE                *post_f                 = 0;
    page_link_layout_t  link_lo                 = {0};
    page_link_layout_t  cur_link_lo             = {0};
    token_t             post_list_toks[MAX_PL_TOKENS];
    token_t             page_toks[MAX_PAGE_TOKENS];
    uint                proj_path_len;

    for (int i = 0; i < argc; ++i) {
        if (streq(argv[i], "-o") || streq(argv[i], "--output")) {
            if (argc < i + 2)
                {err_str = _not_enough_parms_str; ret = 1; goto out;}
            opt_out_dir_path = argv[(i++) + 1];
        } else if (!proj_path) {
            proj_path       = argv[i];
            proj_path_len   = strlen(proj_path);
        } else
            {err_str = _too_many_parms_str; ret = 2; goto out;}
    }

    if (!proj_path)
        {err_str = "project path not specified"; ret = 3; goto out;}

    printf("jeesgen: attempting to generate from directory %s...\n", proj_path);

    if (opt_out_dir_path)
        printf("Output directory specified: %s\n", opt_out_dir_path);

    _read_config(proj_path);

    /*-- Read page link layouts --*/
    dstr_set(&dstr, proj_path);
    dstr_append(&dstr, "/page_link_layout.html");
    if (_page_link_layout_load(&link_lo, dstr))
        {err_str = "failed to read page_link_layout.html"; ret = 3; goto out;}

    dstr_set(&dstr, proj_path);
    dstr_append(&dstr, "/current_page_link_layout.html");
    if (_page_link_layout_load(&cur_link_lo, dstr)) {
        err_str = "failed to read current_page_link_layout.html";
        ret = 4;
        goto out;
    }

    /*-- Read the page layout --*/
    dstr_set(&dstr, proj_path);
    dstr_append(&dstr, "/page_layout.html");
    num_page_toks = _read_page_layout(dstr, &page_layout_str, page_toks);
    if (num_page_toks < 0)
        {err_str ="failed to read page_layout.html"; ret = 3; goto out;}

    /*-- Read the post list layout --*/
    char *index_fp = stack_alloc(proj_path_len + 14);
    if (!index_fp) _panic(-1);
    memcpy(index_fp, proj_path, proj_path_len);
    strcpy(index_fp + proj_path_len, "/post_list_layout.html");

    post_list_layout_str = load_text_file_to_dynamic_str(index_fp);
    if (!post_list_layout_str)
        {err_str = "page layout not found"; ret = 5; goto out;}

    char *head = post_list_layout_str;
    char *t;

    #define FIND_TAG(tag_type, tag_str) \
        t = str_find((post_list_layout_str), (tag_str)); \
        if (t) { \
            post_list_toks[num_post_list_toks].type       = tag_type; \
            post_list_toks[num_post_list_toks].head       = head; \
            post_list_toks[num_post_list_toks].tag_offset = t; \
            head = t + strlen(tag_str); \
            num_post_list_toks++; \
        }
    FIND_TAG(PL_TOK_POSTS, _tag_str_posts);
    FIND_TAG(PL_TOK_POST_PAGE_LINKS, _tag_str_post_page_links);
    #undef FIND_TAG

    assert(num_post_list_toks < MAX_PL_TOKENS);

    post_list_toks[num_post_list_toks].type       = PL_TOK_HTML;
    post_list_toks[num_post_list_toks].head       = head;
    post_list_toks[num_post_list_toks].tag_offset = 0;
    num_post_list_toks++;

    _sort_and_terminate_tokens(post_list_layout_str, post_list_toks,
        num_post_list_toks);

    /*-- Create the generated directory --*/

    if (!opt_out_dir_path) {
        gen_dir_path = dstr_create(proj_path);
        dstr_append(&gen_dir_path,
            gen_dir_path[dstr_len(gen_dir_path) - 1] == '/' ? "generated" :
            "/generated");
    } else
        gen_dir_path = dstr_create(opt_out_dir_path);

    if (create_directory(gen_dir_path)) {
        err_str = "failed to create destination directory"; ret = 6; goto out;
    }

    /*-- Get all files from the project/posts director and sort them --*/
    darr_reserve(posts, 256);

    posts_dir_path = dstr_create(proj_path);
    dstr_append(&posts_dir_path,
        posts_dir_path[get_dynamic_str_len(posts_dir_path) - 1] == '/' ?
        "posts" : "/posts");

    dir_entry_t de;
    dir = open_directory(posts_dir_path, &de);
    if (!dir) {err_str = "failed to open posts directory"; ret = 8; goto out;}

    lluint          post_id;
    char            post_author[256], post_title[256], tmp_buf[256];;
    uint            post_date[3];
    uint            post_time[3];

    for (int de_res = 0; !de_res; de_res = get_next_file_in_directory(dir, &de))
    {
        char *t = str_find(get_dir_entry_name(&de), ".txt");
        if (!t || t[4]) continue; /* Doesn't end with .txt */

        post_t *p = darr_push_empty(posts);
        memset(p, 0, sizeof(post_t));

        p->path = dstr_create_empty(
            get_dynamic_str_len(posts_dir_path) + 2 + \
            strlen(get_dir_entry_name(&de)));

        dstr_set(&p->path, posts_dir_path);
        dstr_append(&p->path, "/");
        dstr_append(&p->path, get_dir_entry_name(&de));

        post_f = fopen(p->path, "r");
        if (!post_f)
            {err_str = "error opening a post"; ret = 10; goto out;}

        bool32 not_ok = 0;

        not_ok |= fscanf(post_f, "id = %llu\n", &post_id) != 1;
        not_ok |= fscanf(post_f, "date = %u-%u-%u %u:%u:%u\n", &post_date[0],
            &post_date[1], &post_date[2], &post_time[0], &post_time[1],
            &post_time[2]) != 6;

        if (not_ok)
            {err_str = "incorrectly formatted post file"; ret = 11; goto out;}
        if (!fgets(tmp_buf, 256, post_f) || strncmp(tmp_buf, "title =", 7))
            {err_str = "post contains no title"; ret = 12; goto out;}
        if (_read_str_until_new_line_or_null(tmp_buf + 7 + 1, post_title, 256))
            {err_str = "post title is invalid"; ret = 13; goto out;}
        if (!fgets(tmp_buf, 256, post_f) || strncmp(tmp_buf, "author =", 8))
            {err_str = "post contains no author"; ret = 14; goto out;}
        if (_read_str_until_new_line_or_null(tmp_buf + 8 + 1, post_author, 256))
            {err_str = "post author is invalid"; ret = 15; goto out;}

        fclose(post_f);
        post_f = 0;

        p->id       = (uint64)post_id;
        p->title    = dstr_create(post_title);
        p->author   = dstr_create(post_author);

        int i;
        for (i = 0; i < 3; ++i) p->date[i] = post_date[i];
        for (i = 0; i < 3; ++i) p->time[i] = post_time[i];
    }

    close_directory(dir);
    dir = 0;

    /*-- Sort posts newest to oldest --*/
    qsort(posts, darr_num(posts), sizeof(post_t), _compare_posts);

    printf("Found %u posts in project.\n", darr_num(posts));

    /* -- Read page files --*/
    _create_page_darr(proj_path, &pages);

    /*-- Write HTML --*/
    if (_generate_post_pages(proj_path, opt_out_dir_path, page_toks,
        num_page_toks, post_list_toks, num_post_list_toks, posts, pages,
        &link_lo, &cur_link_lo))
        {err_str = "failed to generate pages"; ret = 16; goto out;}

    _generate_pages(proj_path, opt_out_dir_path, pages, page_toks,
        num_page_toks, &link_lo, &cur_link_lo);

    if (_cfg.generate_rss) {
        if (!opt_out_dir_path) {
            dstr_set(&dstr, proj_path);
            dstr_append(&dstr, "/generated");
        } else
            dstr_set(&dstr, opt_out_dir_path);
        dstr_append(&dstr, "/feed.rss");
        _generate_rss(posts, dstr);
    }

    out:
        if (ret)
            printf("Generation failed: %s (code %d).\n", err_str, ret);
        else
            printf("Finished generating site for path %s.\n", proj_path);
        if (dir) close_directory(dir);
        if (post_f) fclose(post_f);
        free_dynamic_str(post_list_layout_str);
        free_dynamic_str(gen_dir_path);
        free_dynamic_str(posts_dir_path);
        free_dynamic_str(page_layout_str);
        free_dynamic_str(dstr);
        free_dynamic_str(link_lo.dstr);
        free_dynamic_str(cur_link_lo.dstr);
        for (uint i = 0; i < darr_num(posts); ++i) {
            free_dynamic_str(posts[i].author);
            free_dynamic_str(posts[i].path);
        }
        darr_free(posts);
        darr_free(pages);
        /*-- Destroy _cfg --*/
        free_dynamic_str(_cfg.posts_page_name);
        free_dynamic_str(_cfg.site_link);
        free_dynamic_str(_cfg.site_title);
        free_dynamic_str(_cfg.site_desc);
        free_dynamic_str(_cfg.site_post_dir_addr);
        return ret;
}

static int
_create_page_darr(const char *proj_path, page_darr_t **ret_pages)
{
    int             ret         = 0;
    char            *err_str    = _unknown_str;
    dir_handle_t    dir         = 0;
    dchar           *dstr       = 0;
    FILE            *pf         = 0;
    dir_entry_t     de;

    dstr_set(&dstr, proj_path);
    dstr_append(&dstr, "/pages");

    dir = open_directory(dstr, &de);
    if (!dir) {puts("No pages directory found, skipping..."); goto out;}

    char            *t;
    page_t          *p;
    char            buf[512];

    for (int de_res = 0; !de_res; de_res = get_next_file_in_directory(dir, &de))
    {
        t = str_find(get_dir_entry_name(&de), ".txt");
        if (!t || t[4])
            continue;

        p = darr_push_empty(*ret_pages);
        memset(p, 0, sizeof(page_t));
        p->create_menu_entry = 1;

        /* dstr must be set to the ret_pages directory */
        p->path = dstr_create_empty(
            get_dynamic_str_len(dstr) + 2 + strlen(get_dir_entry_name(&de)));

        dstr_set(&p->path, dstr);
        dstr_append(&p->path, "/");
        dstr_append(&p->path, get_dir_entry_name(&de));

        pf = fopen(p->path, "r");
        if (!pf) {err_str = "error opening a page"; ret = 3; goto out;}

        if (!fgets(buf, 512, pf))
            {err_str = "error reading page"; ret = 4; goto out;}

        if (strncmp(buf, "name = ", 7))
            {err_str = "invalid page file contents"; ret = 5; goto out;}

        char *c = buf;
        for (; *c; ++c) {if (*c == '\n') {*c = 0; break;}};
        dstr_set(&p->name, buf + 7);

        if (dstr_len(p->name) > MAX_PAGE_NAME_LEN) {
            printf("Error: maximum allowed page name length is %d!\n",
                MAX_PAGE_NAME_LEN);
            _panic(-2);
        }

        if (fgets(buf, 512, pf) && !strncmp(buf, "priority = ", 11))
            sscanf(buf, "priority = %d\n", &p->priority);

        if (fgets(buf, 512, pf) && !strncmp(buf, "create menu entry = ", 20)) {
            int val;
            sscanf(buf, "create menu entry = %d\n", &val);
            p->create_menu_entry = val ? 1 : 0;
        }

        fclose(pf);
        pf = 0;

    }

    /*-- Sort ret_pages by priority --*/
    qsort(*ret_pages, darr_num(*ret_pages), sizeof(page_t), _compare_pages);

    out:
        if (ret) printf("Reading pages failed: %s (code %d).\n", err_str, ret);
        if (dir) close_directory(dir);
        if (pf) fclose(pf);
        free_dynamic_str(dstr);
        return ret;
}

static void
_read_config_cb(void *ctx, const char *opt, const char *val)
{
    if (streq(opt, "posts per page")) {
        int v = atoi(val);
        if (v > 0) _cfg.posts_per_page = v;
    } else if (streq(opt, "date format")) {
        dt_unit_t   fmt[3]  = {0};
        int         i       = 0;

        for (const char *c = val; *c && i < 3; ++c, ++i) {
            const char  *oc;
            char        ca, cb;
            int         du_type;
            switch (*c) {
            case 'd': case 'D': ca = 'd'; cb = 'D'; du_type = DU_DAY;   break;
            case 'm': case 'M': ca = 'm'; cb = 'M'; du_type = DU_MONTH; break;
            case 'y': case 'Y': ca = 'm'; cb = 'Y'; du_type = DU_YEAR;  break;
            default: goto invalid_fmt;
            }
            fmt[i].type         = du_type;
            fmt[i].max_chars    = 1;
            for (int j = 0; j < i; ++j)
                if (fmt[j].type == du_type)
                    goto invalid_fmt;
            for (oc = c + 1; (*oc == ca || *oc == cb); ++oc)
                fmt[i].max_chars++;
            c = oc - 1;
        }
        for (int i = 0; i < 3; ++i)
            _cfg.date_format[i] = fmt[i];
        return;

        invalid_fmt:
            puts("Invalid date format specified in jees_config.cfg.");
    } else if (streq(opt, "time format")) {
        int v;
        if (streq(val, "HMS") || streq(val, "0"))
            v = TF_SHOW_HOUR_MIN_SEC;
        else if (streq(val, "HM") || streq(val, "1"))
            v = TF_SHOW_HOUR_MIN;
        else if (streq(val, "H") || streq(val, "2"))
            v  = TF_SHOW_HOUR;
        else {
            puts("Invalid time format specified in jees_config.cfg.");
            return;
        }
        _cfg.time_format = v;
    } else if (streq(opt, "page links per page")) {
        int v = atoi(val);
        if (v >= 0) _cfg.post_page_links_per_page = v;
    } else if (streq(opt, "post page priority"))
        _cfg.post_page_priority = atoi(val);
    else if (streq(opt, "posts page name"))
        dstr_set(&_cfg.posts_page_name, val);
    else if (streq(opt, "use post titles as ids")) {
        bool32 v;
        if (!str_to_bool(val, &v))
            _cfg.use_post_titles_as_ids = v;
    } else if (streq(opt, "max rss items")) {
        int v = atoi(val);
        if (v >= 0) _cfg.max_rss_items = v;
    } else if (streq(opt, "site title"))
        dstr_set(&_cfg.site_title, val);
    else if (streq(opt, "site link"))
        dstr_set(&_cfg.site_link, val);
    else  if (streq(opt, "site description"))
        dstr_set(&_cfg.site_desc, val);
    else if (streq(opt, "max rss description length")) {
        int v = atoi(val);
        _cfg.max_rss_desc_len = v;
    } else if (streq(opt, "site post directory address"))
        dstr_set(&_cfg.site_post_dir_addr, val);
    else if (streq(opt, "generate rss")) {
        bool32 v;
        if (!str_to_bool(val, &v)) _cfg.generate_rss = v;
    } else if (streq(opt, "generate individual post pages")) {
        bool32 v;
        if (!str_to_bool(val, &v))
            _cfg.generate_individual_post_pages = v;
    } else
        printf("Invalid option specified in jees_config.cfg: '%s'.", opt);
}

static void
_read_config(const char *dir_path)
{
    _cfg.posts_per_page = 10;

    _cfg.date_format[0].type        = DU_DAY;
    _cfg.date_format[0].max_chars   = 2;
    _cfg.date_format[0].numeric     = 1;

    _cfg.date_format[1].type        = DU_MONTH;
    _cfg.date_format[1].max_chars   = 2;
    _cfg.date_format[1].numeric     = 0;

    _cfg.date_format[2].type        = DU_YEAR;
    _cfg.date_format[2].max_chars   = 4;
    _cfg.date_format[2].numeric     = 1;

    _cfg.time_format                = TF_SHOW_HOUR_MIN_SEC;
    _cfg.post_page_priority         = 0;
    _cfg.posts_page_name            = dstr_create("news");
    _cfg.use_post_titles_as_ids     = 0;
    _cfg.max_rss_items              = 10;
    _cfg.max_rss_desc_len           = 128;
    _cfg.generate_rss               = 1;

    int     len     = dir_path ? strlen(dir_path) : 0;
    char    *str    = stack_alloc(len + CFG_FILE_NAME_LEN + 2);
    if (!str) _panic(-1);
    if (dir_path) memcpy(str, dir_path, len);
    strcpy(str + len, "/jees_config.cfg");
    parse_cfg_file(str, _read_config_cb, 0);

    printf("Config:\n"
            "    posts per page = %d\n",
           _cfg.posts_per_page);
    printf("    date format = ");
    for (int i = 0; i < 3; ++i) {
        char c;
        for (int j = 0; j < _cfg.date_format[i].max_chars; ++j) {
            switch (_cfg.date_format[i].type) {
            case DU_DAY:    c = 'D'; break;
            case DU_MONTH:  c = 'M'; break;
            case DU_YEAR:   c = 'Y'; break;
            default: assert(0);
            }
            putc(c, stdout);
        }
    }
    puts("");

    char *time_fmt_str;
    switch (_cfg.time_format) {
    case TF_SHOW_HOUR_MIN_SEC:  time_fmt_str = "HMS"; break;
    case TF_SHOW_HOUR_MIN:      time_fmt_str = "HM"; break;
    case TF_SHOW_HOUR:          time_fmt_str = "H"; break;
    default:                    assert(0);
    }
    printf("    time format = %s\n", time_fmt_str);
}

static void
_panic(int code) {exit(code);}

static int
_fwrite_post_content(FILE *f, post_t *p, token_t *post_tokens,
    uint num_post_tokens)
{
    int         ret     = 0;
    long int    offset  = ftell(f);

    dchar *ofs = load_text_file_to_dynamic_str(p->path);
    if (!ofs) {ret = 1; fseek(f, offset, SEEK_SET); goto out;}
    str_purge_trailing_spaces(ofs);

    for (uint k = 0; k < num_post_tokens; ++k) {
        fputs(post_tokens[k].head, f);
        switch (post_tokens[k].type) {
        case POST_TOK_TITLE:  if (p->title)  fputs(p->title, f);    break;
        case POST_TOK_AUTHOR: if (p->author) fputs(p->author, f);   break;
        case POST_TOK_DATE:
            fprintf(f, "%u.%u.%u", p->date[0], p->date[1], p->date[2]);
            break;
        case POST_TOK_TIME:
            fprintf(f, "%02u:%02u:%02u", p->time[0], p->time[1], p->time[2]);
            break;
        case POST_TOK_CONTENT: {
            char *a = str_find(ofs, "[begin_content]");
            char *b = str_find(ofs, "[end_content]");
            if (a && b && b > a)
            {
                *b = 0;
                bool32 last_is_space = 0;
                for (const char *c = a + 15; *c; ++c)
                {
                    if (!last_is_space || *c != ' ')
                        fputc(*c, f);
                    else
                        fputs("&nbsp;", f);
                    last_is_space = *c == ' ';
                }
            }
            /* 15 = strlen([begin_content]) */
        }
            break;
        case POST_TOK_STRING_ID:
            /* In-page id for linking */
            if (_cfg.use_post_titles_as_ids)
                fprintf(f, "post%llu_%s", (lluint)p->id, p->title);
            else
                fprintf(f, "post%llu", (lluint)p->id);
            break;
        case POST_TOK_POST_URL: {
            if (!_cfg.generate_individual_post_pages)
                break;
            if (_cfg.use_post_titles_as_ids) {
                char buf[512];
                if (snprintf(buf, 512, "post%llu_%s", (lluint)p->id,
                    p->title) >= 512)
                    _panic(-1);
                str_strip_non_alpha(str_find(buf, "_") + 1);
                str_to_lower(buf);
                fprintf(f, "\"%s.html\"", buf);
            } else
                fprintf(f, "\"post%llu.html\"", (lluint)p->id);
        }
            break;
        }
    }
    out:
        free_dynamic_str(ofs);
        return ret;
}


static int
_fwrite_individual_post_page_content(FILE *f, post_t *p, uint post_index,
    uint num_posts, token_t *post_toks, uint num_post_toks,
    token_t *ind_post_toks, uint num_ind_post_toks)
{
    int ret = 0;

    for (uint i = 0; i < num_ind_post_toks; ++i) {
        fputs(ind_post_toks[i].head, f);

        switch (ind_post_toks[i].type) {
        case IND_POST_TOK_CONTENT:
            if (_fwrite_post_content(f, p, post_toks, num_post_toks))
                {ret = 1; goto out;}
            break;
        case IND_POST_TOK_BACK_URL: {
            uint page_index = 0;
            if (_cfg.posts_per_page)
                page_index = post_index / _cfg.posts_per_page;
            fprintf(f, "\"post_page%u.html\"", page_index);
        }
            break;
        case IND_POST_TOK_HTML:
            break;
        }
    }

    out:
        {return ret;}

}

static int
_generate_post_pages(const char *proj_path, const char *opt_out_dir_path,
    token_t *page_toks, uint num_page_toks,
    token_t *post_list_toks, uint num_post_list_toks, post_darr_t *posts,
    page_darr_t *pages, page_link_layout_t *link_lo,
    page_link_layout_t *cur_link_lo)
{
    int             ret             = 0;
    dchar           *page_fp        = 0;
    FILE            *f              = 0;
    dchar           *post_lo        = 0;
    dchar           *dstr           = 0;
    dchar           *ind_post_lo    = 0;

    uint32 ppp              = _cfg.posts_per_page;
    uint32 num_tot_pages    = darr_num(posts) / ppp;
    if (darr_num(posts) % ppp)
        num_tot_pages++;
    if (num_tot_pages < 1)
        num_tot_pages = 1;

    uint32 proj_path_len = strlen(proj_path);

    page_fp = create_empty_dynamic_str(proj_path_len + 512);
    if (!page_fp) {ret = 1; goto out;}

    /*-- Load the post layout --*/
    dchar *post_lo_fp = dstr_create(proj_path);
    dstr_append(&post_lo_fp, "/post_layout.html");

    post_lo = load_text_file_to_dynamic_str(post_lo_fp);
    free_dynamic_str(post_lo_fp);

    token_t         post_tokens[MAX_POST_TOKS]  = {0};
    uint            num_post_tokens             = 0;
    char            *head                       = post_lo;
    char            *tag_offset;

    #define FIND_TOK(type_id, tag_str) \
        tag_offset = str_find(post_lo, (tag_str)); \
        if (tag_offset) { \
            post_tokens[num_post_tokens].head         = head; \
            post_tokens[num_post_tokens].type         = type_id; \
            post_tokens[num_post_tokens].tag_offset   = tag_offset; \
            head = tag_offset + strlen(tag_str); \
            num_post_tokens++; \
        }
    FIND_TOK(POST_TOK_TITLE,        _tag_str_title);
    FIND_TOK(POST_TOK_AUTHOR,       _tag_str_author);
    FIND_TOK(POST_TOK_DATE,         _tag_str_date);
    FIND_TOK(POST_TOK_TIME,         _tag_str_time);
    FIND_TOK(POST_TOK_CONTENT,      _tag_str_content);
    FIND_TOK(POST_TOK_STRING_ID,    _tag_str_post_string_id);
    FIND_TOK(POST_TOK_POST_URL,     _tag_str_post_url);
    post_tokens[num_post_tokens].type       = POST_TOK_HTML;
    post_tokens[num_post_tokens].head       = head;
    post_tokens[num_post_tokens].tag_offset = 0;
    num_post_tokens++;
    #undef FIND_TOK

    _sort_and_terminate_tokens(post_lo, post_tokens, num_post_tokens);

    /*-- Write each page --*/
    char buf[512];

    for (uint32 i = 0; i < num_tot_pages; ++i) {
        uint32 num_posts    = MIN(darr_num(posts) - i * ppp, ppp);
        uint32 fi           = i * ppp;

        if (!opt_out_dir_path) {
            if (snprintf(buf, 512,
                "/generated/post_page%u.html", i) >= 512)
                _panic(-1);
            dstr_set(&page_fp, proj_path);
        } else {
            if (snprintf(buf, 512, "/post_page%u.html", i) >= 512)
                _panic(-1);
            dstr_set(&page_fp, opt_out_dir_path);
        }
        dstr_append(&page_fp, buf);

        f = fopen(page_fp, "w+");
        if (!f) {ret = 4; goto out;}

        for (uint j = 0; j < num_page_toks; ++j) {
            fputs(page_toks[j].head, f);

            switch (page_toks[j].type) {
            case PAGE_TOK_CONTENT:
            {
                for (uint k = 0; k < num_post_list_toks; ++k) {
                    fputs(post_list_toks[k].head, f);

                    switch (post_list_toks[k].type) {
                    case PL_TOK_POSTS:
                    {
                        for (uint32 h = fi; h < fi + num_posts; ++h) {
                            int r = _fwrite_post_content(f,
                                &posts[h], post_tokens, num_post_tokens);
                            if (r) {ret = 5; goto out;}
                        }
                    }
                        break;
                    case PL_TOK_POST_PAGE_LINKS: {
                        int num_pages = MIN((int)num_tot_pages,
                            (int)_cfg.post_page_links_per_page);
                        if (num_pages <= 0)
                            break;
                        int ii              = (int)i;
                        int per_page_half   = num_pages / 2;
                        int pfirst          = ii - per_page_half;
                        int first = CLAMP(pfirst, 0,
                            (int)num_tot_pages - num_pages);
                        int plast = first + _cfg.post_page_links_per_page;
                        int last = MIN(plast, (int)num_tot_pages);
                        for (int k = first; k < last; ++k) {
                            if (k != ii)
                                fprintf(f,
                                    "<A HREF=\"post_page%d.html\">%d</A> ", k,
                                    k);
                            else
                                fprintf(f, "%d ", k);
                        }
                    }
                        break;
                    case PL_TOK_HTML:
                        break;
                    }
                }
            }
                break;
            case PAGE_TOK_PAGE_LINKS:
                _fwrite_page_links(f, pages, 0, link_lo, cur_link_lo);
                break;
            }
        }
        fclose(f);
        f = 0;
    }

    /*-- Generate individual post pages if specified in config --*/

    if (!_cfg.generate_individual_post_pages)
        goto out;

    dstr_set(&dstr, proj_path);
    dstr_append(&dstr, "/individual_post_layout.html");
    ind_post_lo = load_text_file_to_dynamic_str(dstr);
    if (!ind_post_lo) {
        puts("Error: option 'generate individual post pages' specified, but "
            "failed to find load file 'individual_post_layout.html'.");
        goto out;
    }

    token_t ind_post_toks[MAX_IND_POST_TOKS];
    uint    num_ind_post_toks = 0;
    head = ind_post_lo;
    #define FIND_TOK(type_id, tag_str) \
        tag_offset = str_find(ind_post_lo, (tag_str)); \
        if (tag_offset) { \
            ind_post_toks[num_ind_post_toks].head         = head; \
            ind_post_toks[num_ind_post_toks].type         = type_id; \
            ind_post_toks[num_ind_post_toks].tag_offset   = tag_offset; \
            head = tag_offset + strlen(tag_str); \
            num_ind_post_toks++; \
        }
    FIND_TOK(IND_POST_TOK_CONTENT, _tag_str_content);
    FIND_TOK(IND_POST_TOK_BACK_URL, _tag_str_back_url);
    ind_post_toks[num_ind_post_toks].type       = IND_POST_TOK_HTML;
    ind_post_toks[num_ind_post_toks].head       = head;
    ind_post_toks[num_ind_post_toks].tag_offset = 0;
    num_ind_post_toks++;
    _sort_and_terminate_tokens(ind_post_lo, ind_post_toks, num_ind_post_toks);
    #undef FIND_TOK

    int gen_post_err = 0;

    for (uint32 i = 0; i < darr_num(posts); ++i) {
        post_t  *p          = &posts[i];
        dchar   *post_str   = load_text_file_to_dynamic_str(p->path);

        if (!post_str)
            {gen_post_err = 1; goto gen_post_end;}

        if (!opt_out_dir_path) {
            dstr_set(&page_fp, proj_path);
            dstr_append(&page_fp, "/generated/");
        } else {
            dstr_set(&page_fp, opt_out_dir_path);
            dstr_append(&page_fp, "/");
        }

        int r;
        if (_cfg.use_post_titles_as_ids)
            r = snprintf(buf, 512, "post%llu_%s", (lluint)p->id, p->title);
        else
            r = snprintf(buf, 512, "post%llu", (lluint)p->id);

        if (r >= 512)
            _panic(-1);

        if (_cfg.use_post_titles_as_ids) {
            str_strip_non_alpha(str_find(buf, "_") + 1);
            str_to_lower(buf);
        }

        dstr_append(&page_fp, buf);
        dstr_append(&page_fp, ".html");

        f = fopen(page_fp, "w+");
        if (!f)
            {gen_post_err = 2; goto gen_post_end;}

        for (uint32 j = 0; j < num_page_toks; ++j) {
            fputs(page_toks[j].head, f);

            switch (page_toks[j].type) {
            case PAGE_TOK_CONTENT:
                if (_fwrite_individual_post_page_content(f, p, i,
                    darr_num(posts), post_tokens, num_post_tokens,
                    ind_post_toks, num_ind_post_toks))
                    {gen_post_err = 3; goto gen_post_end;}
                break;
            case PAGE_TOK_PAGE_LINKS:
                /* Pass in the "non current" link layout for both parms */
                _fwrite_page_links(f, pages, 0, link_lo, link_lo);
                break;
            case PAGE_TOK_HTML:
                break;
            }
        }

        gen_post_end:
            if (gen_post_err)
                printf("%s: gen post error %d.\n", __func__, gen_post_err);
            free_dynamic_str(post_str);
            if (f)
                fclose(f);
            f = 0;

    }

    out:
        free_dynamic_str(page_fp);
        free_dynamic_str(post_lo);
        free_dynamic_str(dstr);
        free_dynamic_str(ind_post_lo);
        if (f) fclose(f);
        if (ret) printf("Build pages: error code %d.\n", ret);
        return ret;
}

static int
_generate_pages(const char *proj_path, const char *opt_out_dir_path,
    page_darr_t *pages, token_t *page_toks, uint num_page_toks,
    page_link_layout_t *link_lo, page_link_layout_t *cur_link_lo)
{
    int             ret         = 0;
    char            *err_str    = _unknown_str;
    dir_handle_t    dir         = 0;
    dchar           *dstr       = 0;
    FILE            *pf         = 0;
    dchar           *page_str   = 0;
    char            page_name_lower[MAX_PAGE_NAME_LEN + 1];
    page_t          *p;

    /*-- Generate HTML for pages --*/
    uint    i, j;
    token_t *tok;

    for (i = 0; i < darr_num(pages); ++i) {
        p = &pages[i];

        if (!opt_out_dir_path) {
            dstr_set(&dstr, proj_path);
            dstr_append(&dstr, "/generated");
        } else
            dstr_set(&dstr, opt_out_dir_path);

        dstr_append(&dstr, "/");
        strcpy(page_name_lower, p->name);
        str_to_lower(page_name_lower);
        dstr_append(&dstr, page_name_lower);
        dstr_append(&dstr, ".html");

        pf = fopen(dstr, "w+");
        if (!pf)
            {err_str = "failed to create new page file"; ret = 7; goto out;}

        page_str = load_text_file_to_dynamic_str(p->path);
        if (!page_str) {
            err_str = "failed to read page while writing it";
            ret = 8;
            goto out;
        }

        for (j = 0; j < num_page_toks; ++j) {
            tok = &page_toks[j];
            fputs(page_toks[j].head, pf);

            switch (tok->type) {
            case PAGE_TOK_CONTENT: {
                char *a = str_find(page_str, "[begin_content]");
                char *b = str_find(page_str, "[end_content]");
                if (!(a && b && a < b)) break;
                *b = 0;
                fputs(a + 15, pf);
            }
                break;
            case PAGE_TOK_PAGE_LINKS:
                _fwrite_page_links(pf, pages, p, link_lo, cur_link_lo);
                break;
            case PAGE_TOK_HTML:
                break;
            }
        }
        fclose(pf);
        pf = 0;
        dstr_free(&page_str);
    }
    out:
        if (dir) close_directory(dir);
        free_dynamic_str(dstr);
        free_dynamic_str(page_str);
        if (pf) fclose(pf);
        if (ret) printf("Generate pages: %s (code %d).", err_str, ret);
        return ret;
}

static int
_read_str_until_new_line_or_null(char *str, char *buf, int buf_len)
{
    int     len = 0;
    char    *c;
    for (c = str; *c && *c != '\n'; ++c) {
        if (len == buf_len - 1) break;
        buf[len++] = *c;
    }
    if (*c != '\n' && *c) return 1;
    buf[len] = 0;
    return 0;
}

static int
_null_terminate_str_at_first_new_line(char *s)
{
    if (!s)
        return 0;
    char *tmp_s = s;
    for (; *tmp_s; ++tmp_s) {
        if (*tmp_s != '\n')
            continue;
        *tmp_s = 0;
        break;
    }
    return (int)(size_t)(tmp_s - s);
}

static int
_post(const char *proj_dp, const char *post_fp)
{
    int     ret         = 0;
    dchar   *dstr       = 0;
    dchar   *post       = 0;
    char    *err_str    = _unknown_str;
    FILE    *f          = 0;
    char    title[256];
    char    author[256];
    char    *tmp, *content_begin;
    cache_t cache;

    post = load_text_file_to_dynamic_str(post_fp);
    if (!post)
        {err_str = "failed to load post file"; ret = 1; goto out;}

    /*-- Find the title at the beginning of the post --*/
    if (strncmp(post, "title = ", 8))
        {err_str = "post contains no title"; ret = 2; goto out;}

    tmp = post + 8;

    if (_read_str_until_new_line_or_null(tmp, title, 256))
        {err_str = "post title is invalid"; ret = 2; goto out;}

    while (*tmp && *tmp != '\n') tmp++;
    if (*tmp == '\n') tmp++;

    /*-- Now find the author --*/
    if (strncmp(tmp, "author = ", 9))
        {err_str = "post contains no author"; ret = 2; goto out;}

    tmp += 9; /* strlen("author = " */

    if (_read_str_until_new_line_or_null(tmp, author, 256))
        {err_str = "post author is invalid"; ret = 3; goto out;}

    /*-- Check if the content begins at a null terminator --*/
    content_begin = tmp + strlen(author);
    if (*content_begin) content_begin++;

    dstr_set(&dstr, proj_dp);
    dstr_append(&dstr, "/jees_cache.dat");

    if (_read_cache(&cache, dstr))
        {err_str = "failed to read jees_cache.dat"; ret = 4; goto out;}

    char buf[64];
    if (snprintf(buf, 64, "/posts/post%llu.txt",
        (lluint)cache.running_post_id) >= 64)
        {ret = 5; goto out;}

    dstr_set(&dstr, proj_dp);
    dstr_append(&dstr, buf);

    f = fopen(dstr, "w+");
    if (!f) {err_str= "failed creating post file"; ret = 6; goto out;}

    fprintf(f, "id = %llu\n", (lluint)cache.running_post_id);

    struct tm *t = get_local_time_info();
    fprintf(f, "date = %d-%d-%d %d:%d:%d\n", t->tm_mday,
        t->tm_mon + 1, /* Months start from 0... */
        t->tm_year + 1900, t->tm_hour, t->tm_min, t->tm_sec);
    fprintf(f, "title = %s\n", title);
    fprintf(f, "author = %s\n", author);
    fputs("\n[begin_content]\n", f);
    fputs(content_begin, f);
    fputs("[end_content]", f);

    fclose(f);
    f = 0;

    cache.running_post_id++;
    dstr_set(&dstr, proj_dp);
    dstr_append(&dstr, "/jees_cache.dat");
    _save_cache(&cache, dstr);

    out:
        if (f)
            fclose(f);
        free_dynamic_str(post);
        free_dynamic_str(dstr);
        if (ret)
            printf("Post failed: %s (code %d).\n", err_str, ret);
        else
            printf("Post successful (id %llu):\n  Title: %s\n  Author: %s\n",
                (lluint)cache.running_post_id - 1, title, author);
        return ret;
}

static int
_new(const char *fp)
{
    const char  *err_str    = _unknown_str;
    int         ret         = 0;
    FILE        *f          = 0;
    dchar       *dstr       = 0;

    /*-- Create project root directory --*/
    if (file_exists(fp))
        {err_str = "file or directory already exists"; ret = 1; goto out;}

    if (create_directory(fp))
        {err_str = "failed creating directory"; ret = 2; goto out;}

    dstr = create_dynamic_str(fp);
    if (!dstr) {err_str = _nomem_str; ret = 3; goto out;}

    /*-- Create the "posts" directory --*/
    dstr_append(&dstr,
        dstr[get_dynamic_str_len(dstr) - 1] == '/' ? "posts" : "/posts");

    if (create_directory(dstr))
        {err_str = "failed to create the posts directory"; ret = 5; goto out;}

    /*-- Create post_list_layout.html --*/
    dstr_set(&dstr, fp);
    dstr_append(&dstr, "/post_list_layout.html");
    f = fopen(dstr, "w+");
    if (!f) {
        err_str = "failed to create post_list_layout.html";
        ret     = 7;
        goto out;
    }
    fputs("<DIV>\n", f);
    fputs(_tag_str_posts, f);
    fprintf(f, "\n<DIV>%s</DIV>\n", _tag_str_post_page_links);
    fputs("</DIV>", f);
    fclose(f);
    f = 0;

    /*-- Create post_layout.html --*/
    dstr_set(&dstr, fp);
    dstr_append(&dstr, "/post_layout.html");
    f = fopen(dstr, "w+");
    if (!f)  {err_str = "failed to create post_layout.html"; ret = 8; goto out;}
    fprintf(f, "<DIV>\n");
    fputs("<DIV>\n", f);
    fprintf(f, "<A HREF=%s>%s</A>", _tag_str_post_url, _tag_str_title);
    fprintf(f, "%s ", _tag_str_author);
    fprintf(f, "<DIV id=\"%s\">%s</DIV> ", _tag_str_post_string_id,
        _tag_str_date);
    fprintf(f, _tag_str_time);
    fputs("\n</DIV>\n<DIV>\n", f);
    fprintf(f, _tag_str_content);
    fputs("\n</DIV>\n</DIV>\n", f);
    fclose(f);
    f = 0;

    /*-- Create page_layout.html --*/
    dstr_set(&dstr, fp);
    dstr_append(&dstr, "/page_layout.html");
    f = fopen(dstr, "w+");
    if (!f)  {err_str = "failed to create page_layout.html"; ret = 9; goto out;}
    fputs("<HTML>\n<HEAD>\n</HEAD>\n<BODY>\n", f);
    fputs(_tag_str_content, f);
    fprintf(f, "\n<DIV>%s</DIV>\n", _tag_str_page_links);
    fputs("</BODY>\n</HTML>", f);
    fclose(f);
    f = 0;

    /*-- Create page_link_layout.html --*/
    dstr_set(&dstr, fp);
    dstr_append(&dstr, "/page_link_layout.html");
    f = fopen(dstr, "w+");
    if (!f) {
        err_str = "failed to create page_lnk_layout.html";
        ret     = 10;
        goto out;
    }
    fprintf(f, "<A HREF=%s><DIV>%s</DIV></A>", _tag_str_link_path,
        _tag_str_link_name);
    fclose(f);
    f = 0;

    /*-- Create current_page_link_layout.html --*/
    dstr_set(&dstr, fp);
    dstr_append(&dstr, "/current_page_link_layout.html");
    f = fopen(dstr, "w+");
    if (!f) {
        err_str = "failed to create page_link_layout.html";
        ret     = 11;
        goto out;
    }
    fprintf(f, "<DIV>%s</DIV>", _tag_str_link_name);
    fclose(f);
    f = 0;

    /*-- Create individual_post_layout.html --*/
    dstr_set(&dstr, fp);
    dstr_append(&dstr, "/individual_post_layout.html");
    f = fopen(dstr, "w+");
    if (!f) {
        err_str ="failed to create individual_post_layout.html";
        ret     = 12;
        goto out;
    }
    fprintf(f, "<DIV><A HREF=%s>Back</A></DIV>\n", _tag_str_back_url);
    fprintf(f, _tag_str_content);
    fclose(f);
    f = 0;

    /*-- Create jees_config.cfg --*/
    dstr_set(&dstr, fp);
    dstr_append(&dstr, "/jees_config.cfg");

    f = fopen(dstr, "w+");
    if (!f) {err_str = "failed to create jees_config.cfg"; ret = 9; goto out;}
    fputs(_default_cfg_str, f);
    fclose(f);
    f = 0;

    /*-- Create jees_cache.dat --*/
    dstr_set(&dstr, fp);
    dstr_append(&dstr, "/jees_cache.dat");

    f = fopen(dstr, "w+");
    if (!f) {err_str = "failed to create jees_cache.cfg"; ret = 9; goto out;}
    fputs("running post id = 0", f);
    fclose(f);
    f = 0;

    out:
        if (ret)
            printf("Project creation failed: %s (code %d).\n", err_str, ret);
        else
            printf("Successfully created new project %s.\n", fp);
        if (f)
            fclose(f);
        free_dynamic_str(dstr);
        return ret;
}

static void
_read_cache_cb(void *ctx, const char *opt, const char *val)
{
    cache_t *c = ctx;
    if (streq(opt, "running post id"))
        c->running_post_id = str_to_uint64(val);
    else
        printf("Invalid option in jees_cache.dat: %s.\n", opt);
}

static int
_read_cache(cache_t *c, const char *fp)
{
    memset(c, 0, sizeof(cache_t));
    if (parse_cfg_file(fp, _read_cache_cb, c))
        {puts("jees_cache.dat not found or invalid."); return 1;}
    return 0;
}

static int
_save_cache(cache_t *cache, const char *fp)
{
    FILE *f = fopen(fp, "w+");
    if (!f)
        return 1;
    fprintf(f, "running post id = %llu\n", (lluint)cache->running_post_id);
    fclose(f);
    return 0;
}

static int
_compare_tokens(const void *a, const void *b)
{
    const token_t *ta = a;
    const token_t *tb = b;
    if (!ta->tag_offset) return 1;
    if (!tb->tag_offset) return -1;
    return ta->tag_offset < tb->tag_offset ? -1 : 1;
}

static int
_compare_posts(const void *a, const void *b)
{
    const post_t *pa = a;
    const post_t *pb = b;
    return pa->id > pb->id ? -1 : 1;
}

static int
_compare_pages(const void *a, const void *b)
{
    const page_t *pa = a;
    const page_t *pb = b;
    return pa->priority >= pb->priority ? -1 : 1;
}


static int
_read_page_layout(const char *fp, dchar **ret_lo,
    token_t ret_toks[MAX_PAGE_TOKENS])
{
    puts(fp);
    dchar *lo = load_text_file_to_dynamic_str(fp);
    if (!lo) return -1;

    int     num     = 0;
    char    *head   = lo;
    char    *tag_offset;
    #define FIND_TOK(type_id, tag_str) \
        tag_offset = str_find(lo, (tag_str)); \
        if (tag_offset) \
        { \
            ret_toks[num].head          = head; \
            ret_toks[num].type          = type_id; \
            ret_toks[num].tag_offset    = tag_offset; \
            head                        = tag_offset + strlen(tag_str); \
            num++; \
        }
    FIND_TOK(PAGE_TOK_CONTENT, _tag_str_content);
    FIND_TOK(PAGE_TOK_PAGE_LINKS, _tag_str_page_links);
    #undef FIND_TOK

    ret_toks[num].type       = PAGE_TOK_HTML;
    ret_toks[num].head       = head;
    ret_toks[num].tag_offset = 0;
    num++;

    _sort_and_terminate_tokens(lo, ret_toks, num);
    *ret_lo = lo;
    return num;
}

static void
_sort_and_terminate_tokens(char *str, token_t *toks, int num)
{
    qsort(toks, num, sizeof(token_t), _compare_tokens);
    int i;
    toks[0].head = str;

    /*-- Find heads --*/
    for (i = 1; i < num; ++i) {
        toks[i].head = toks[i - 1].tag_offset;
        for (;*toks[i].head != '>'; ++toks[i].head);
        toks[i].head++;
    }

    /*-- Null-terminate --*/
    for (i = 0; i < num; ++i)
        if (toks[i].tag_offset) *toks[i].tag_offset = 0;
}

static int
_new_page(int argc, char **argv)
{
    char    *err_str            = _unknown_str;
    dchar   *page_str           = 0;
    dchar   *dstr               = 0;
    int     ret                 = 0;
    FILE    *f                  = 0;
    int     priority            = 0;
    bool32  create_menu_entry   = 1;
    char    *page_name          = 0;
    char    buf[256];

    if (argc > 2)
        {err_str = "too many parameters"; ret = 1; goto out;}

    if (!(page_str = load_text_file_to_dynamic_str(argv[1])))
        {err_str = "could not open page file"; ret = 2; goto out;}

    int     len         = strlen(page_str);
    char    *str_cur    = page_str;

    for (;;) {
        if (!strncmp(str_cur, "name = ", 7)) {
            page_name = str_cur + 7;
            str_cur += _null_terminate_str_at_first_new_line(str_cur);
            if (str_cur != page_str + len)
                str_cur++;
        } else if (!strncmp(str_cur, "priority = ", 11)) {
            _read_str_until_new_line_or_null(str_cur, buf, 512);
            if (strlen(buf))
                priority = atoi(buf + 11);
            str_cur += _null_terminate_str_at_first_new_line(str_cur);
            if (str_cur != page_str + len)
                str_cur++;
        } else if (!strncmp(str_cur, "create menu entry = ", 17)) {
            char *opt_str = str_cur + 17;
            str_cur = opt_str + _null_terminate_str_at_first_new_line(opt_str);
            if (str_cur != page_str + len)
                str_cur++;
            str_to_bool(opt_str, &create_menu_entry);
        } else
            break;
    }

    if (!page_name)
        {err_str = "page name is invalid"; ret = 3; goto out;}

    printf("Adding page '%s', priority %d, create link: %d\n", page_name,
        priority, create_menu_entry);

    /*-- Save page --*/
    dstr_set(&dstr, argv[0]);
    dstr_append(&dstr, "/pages");

    if (create_directory(dstr))
        {err_str = "could not create pages directory"; ret = 4; goto out;}

    dstr_append(&dstr, "/");
    dstr_append(&dstr, page_name);
    dstr_append(&dstr, ".txt");

    if (!(f = fopen(dstr, "w+")))
        {err_str = "could not create page file"; ret = 5; goto out;}

    fputs(page_name - 7, f); /* 7 = strlen("name = ") */
    fprintf(f, "\npriority = %d", priority);
    fprintf(f, "\ncreate menu entry = %d\n", create_menu_entry);
    fputs("\n[begin_content]\n", f);
    fputs(str_cur, f);
    fputs("\n[end_content]\n", f);

    out:
        free_dynamic_str(page_str);
        free_dynamic_str(dstr);
        if (ret)
            printf("Adding page failed: %s (code %d).\n", err_str, ret);
        else
            printf("Successfully created page '%s', priority %d, create menu "
                "entry: %d.\n", page_name, priority, create_menu_entry);
        if (f)
            fclose(f);
    return ret;
}

/* TODO: rename to page links to menu entries */
static int
_fwrite_page_links(FILE *f, page_darr_t *pages, page_t *page,
    page_link_layout_t *link_lo, page_link_layout_t *cur_link_lo)
{
    bool32              wrote_posts = 0;
    uint                i;
    int                 j;
    page_link_layout_t  *lo;
    char                *name;
    char                *fn;
    char                page_name_lower[MAX_PAGE_NAME_LEN + 1];

    for (i = 0; i < darr_num(pages) || !wrote_posts; ++i) {
        bool32 writing_posts = 0;

        if (i == darr_num(pages)
        || (!wrote_posts && _cfg.post_page_priority > pages[i].priority)) {
            name            = _cfg.posts_page_name;
            fn              = "post_page0";
            wrote_posts     = 1;
            writing_posts   = 1;
            if (i < darr_num(pages))
                i--;
        } else {
            if (!pages[i].create_menu_entry)
                continue;
            name    = pages[i].name;
            fn      = page_name_lower;
            strcpy(page_name_lower, name);
            str_to_lower(page_name_lower);
        }

        if (&pages[i] == page || (!page && writing_posts))
            lo = cur_link_lo;
        else
            lo = link_lo;

        for (j = 0; j < lo->num_toks; ++j) {
            fputs(lo->toks[j].head, f);
            switch (lo->toks[j].type)
            {
            case LINK_TOK_PATH: {
                fprintf(f, "\"%s", fn);
                fputs(".html\"", f);
            }
                break;
            case LINK_TOK_NAME: fputs(name, f); break;
            case LINK_TOK_HTML: fputc('\n', f); break;
            }
        }
    }
    return 0;
}

static int
_page_link_layout_load(page_link_layout_t *lo, const char *fp)
{
    memset(lo, 0, sizeof(page_link_layout_t));
    lo->dstr = load_text_file_to_dynamic_str(fp);
    if (!lo->dstr) return 1;

    char *head = lo->dstr;
    char *tag_offset;

    #define FIND_TOK(type_id, tag_str) \
        tag_offset = str_find(lo->dstr, (tag_str)); \
        if (tag_offset) { \
            lo->toks[lo->num_toks].head         = head; \
            lo->toks[lo->num_toks].type         = type_id; \
            lo->toks[lo->num_toks].tag_offset   = tag_offset; \
            head = tag_offset + strlen(tag_str); \
            lo->num_toks++; \
        }
    FIND_TOK(LINK_TOK_PATH, _tag_str_link_path);
    FIND_TOK(LINK_TOK_NAME, _tag_str_link_name);
    #undef FIND_TOK

    lo->toks[lo->num_toks].type         = LINK_TOK_HTML;
    lo->toks[lo->num_toks].head         = head;
    lo->toks[lo->num_toks].tag_offset   = 0;
    ++lo->num_toks;

    _sort_and_terminate_tokens(lo->dstr, lo->toks, lo->num_toks);
    return 0;
}

static int
_generate_rss(post_darr_t *posts, const char *fp)
{
    FILE *f = fopen(fp, "w+");
    if (!f) return 1;
    char str_buf[256];

    fputs("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
        "<rss version=\"2.0\">\n"
        "<channel>\n", f);
    fprintf(f, "    <title>%s</title>\n",
        _cfg.site_title ? _cfg.site_title : _empty_str);
    fprintf(f, "    <description>%s</description>\n"
        "    <link>%s</link>\n", _cfg.site_desc ? _cfg.site_desc: _empty_str,
        _cfg.site_link ? _cfg.site_link : _empty_str);

    if (darr_num(posts) > 0) {
        uint num = MIN(_cfg.max_rss_items, darr_num(posts));
        uint beg = num - MIN(_cfg.max_rss_items, num);

        int     numeric_week_day;
        char    month_str[4]        = {0};
        char    week_day_str[4]     = {0};
        char    month_nday_str[3]   = {0};
        uint    page_num;
        post_t  *p;

        /*-- Write the last build date --*/
        struct tm *t = get_local_time_info();
        uint year = t->tm_year + 1900;

        numeric_week_day = week_day_from_date(t->tm_mday, t->tm_mon + 1, year);
        assert(numeric_week_day >= 0);
        strncpy(month_str, month_to_str(CLAMP(t->tm_mon + 1, 1, 12)), 3);
        strncpy(week_day_str, week_day_to_str(numeric_week_day), 3);
        uint_to_zero_padded_str(t->tm_mday, 3, month_nday_str);

        fprintf(f, "    <lastBuildDate>%s, %02d, %s, %u, %02u:%02u:%02u"
            "</lastBuildDate>\n", week_day_str, t->tm_mon + 1, month_str, year,
            t->tm_hour, t->tm_min, t->tm_sec);

        for (uint i = beg; i < num; ++i) {
            p           = &posts[i];
            page_num    = i / _cfg.posts_per_page;

            numeric_week_day = week_day_from_date(p->date[0], p->date[1],
                p->date[2]);
            assert(numeric_week_day >= 0);
            strncpy(month_str, month_to_str(CLAMP(p->date[1], 1, 12)), 3);
            strncpy(week_day_str, week_day_to_str(numeric_week_day), 3);
            uint_to_zero_padded_str(p->date[0], 3, month_nday_str);

            fputs("    <item>\n", f);
            fprintf(f, "        <title>%s</title>\n", p->title);
            fputs("        <description>\n", f);
            /* Create description here */
            fputs("        </description>\n", f);

            if (_cfg.site_post_dir_addr) {
                fprintf(f, "        <link>%s", _cfg.site_post_dir_addr);
                if (_cfg.generate_individual_post_pages) {
                    fprintf(f, "/post%llu", (lluint)p->id);
                    if (_cfg.use_post_titles_as_ids) {
                        strncpy(str_buf, p->title, 256);
                        str_strip_non_alpha(str_buf);
                        str_to_lower(str_buf);
                        fputc('_', f);
                        fputs(str_buf, f);
                    }
                    fputs(".html", f);
                } else {
                    fprintf(f, "/post_page%u.html#", page_num);
                    fprintf(f, "post%llu", (lluint)p->id);
                    if (_cfg.use_post_titles_as_ids) {
                        strncpy(str_buf, p->title, 256);
                        str_strip_non_alpha(str_buf);
                        str_to_lower(str_buf);
                        fputc('_', f);
                        fputs(str_buf, f);
                        fputs(str_buf, f);
                    }
                }
                fputs("</link>\n", f);
            }
            fprintf(f, "        <guid>%llu</guid>\n", (lluint)p->id);
            fprintf(f,
                "        <pubDate>%s, %s %s %u %u:%u:%u +0000</pubDate>\n",
                week_day_str, month_nday_str, month_str, p->date[2], p->time[0],
                p->time[1], p->time[2]);
            fputs("    </item>\n", f);
        }
        fputs("</channel>\n</rss>", f);
    }
    return 0;
}
