#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include "utils.h"

#define STRIP_STR(str, call) \
    char *ca = str; \
    char *cb = str; \
    for (;*cb;) {*ca = *(cb++); if (call(*ca)) ca++;} \
    *ca = 0;

void *
emalloc(size_t sz)
{
    void *ret = malloc(sz);
    if (!ret && sz)
        exit(EXIT_FAILURE);
    return ret;
}

void *
erealloc(void *p, size_t sz)
{
    void *ret = realloc(p, sz);
    if (!ret && sz)
        exit(EXIT_FAILURE);
    return ret;
}

void *
darr_ensure_growth_by(void *darr, uint32 grow_by, uint32 item_sz)
{
    if (!grow_by)
        return darr;
    darr_head_t *h = darr_head(darr);
    if (!h) {
        uint32 cap = grow_by > 4 ? grow_by : 4;
        h = emalloc(sizeof(darr_head_t) + cap * item_sz);
        h->num = 0;
        h->cap = 4;
        return h + 1;
    }
    uint32 req = h->num + grow_by;
    if (req <= h->cap)
        return darr;
    uint32 new_cap = h->cap * 2;
    new_cap = new_cap > req ? new_cap : req;
    h = erealloc(h, sizeof(darr_head_t) + new_cap * item_sz);
    h->cap = new_cap;
    return h + 1;
}

char *
load_file_to_str(const char *path)
{
    if (!path) return 0;

    char        *str;
    FILE        *file;
    long int    num_chars;

    file = fopen(path, "rb");
    if (!file) return 0;

    /* Get file length */
    fseek(file, 0L, SEEK_END);
    num_chars = ftell(file);
    rewind(file);

    str = (char*)malloc((num_chars + 1));
    fread(str, sizeof(char), num_chars, file);
    str[num_chars] = 0;
    fclose(file);

    return str;
}

char *
load_text_file_to_dynamic_str(const char *path)
{
    FILE *f = fopen(path, "r");
    if (!f) return 0;

    char *str = 0;

    size_t  len = 0;
    int     c;
    for (c = fgetc(f); c != EOF; c = fgetc(f))
        len++;
    if (!len) {puts(":D");goto out;}

    rewind(f);

    str = create_empty_dynamic_str(len);
    if (!str) goto out;

    char *rc = str;
    while ((*(rc++) = fgetc(f)) != EOF);
    *rc = 0;
    set_dynamic_str_len(str, len);

    out:
        fclose(f);
        return str;
}

void
str_strip_symbols(char *str, const char *symbols, int num_symbols)
{
    if (str && symbols && num_symbols > 0) {
        int len = strlen(str);

        const char *s, *ls;
        ls = symbols + num_symbols;

        for (char *c = str; *c; ++c) {
            for (s = symbols; s < ls; ++s) {
                if (*c != *s)
                    continue;
                /* Why does strcpy not work here on gcc? */
                memcpy(c, c + 1, len - (int)(c - str));
                c--;
                len--;
                break;
            }
        }
    }
}

int
str_strip_symbolsn(char *str, const char *symbols, int num_symbols,
    int str_len)
{
    if (str && symbols && num_symbols > 0 && str_len > 0) {
        int         len = str_len;
        const char  *s, *ls, *lc;

        lc = str + len;
        ls = symbols + num_symbols;

        for (char *c = str; c < lc && *c; ++c) {
            for (s = symbols; s < ls; ++s) {
                if (*c != *s)
                    continue;
                memcpy(c, c + 1, len - (int)(c - str));
                c--;
                len--;
                lc = str + len;
            }
        }
        return len;
    }
    return str_len;
}

void
str_strip_undef_symbols(char *str, const char *symbols, int num_symbols)
{
    if (!str || !symbols || num_symbols <= 0)
        return;

    const char  *s, *ls;
    bool32      is_sym_one_of;
    ls = symbols + num_symbols;

    for (char *c = str; *c; ++c) {
        is_sym_one_of = 0;
        for (s = symbols; s < ls; ++s) {
            if (*c != *s)
                continue;
            is_sym_one_of = 1;
            break;
        }
        if (!is_sym_one_of) {
            strcpy(c, c + 1);
            c--;
        }
    }
}

void
str_strip_non_alpha(char *str)
    {if (str) {STRIP_STR(str, isalpha)};}

bool32
str_contains_one_or_more_of_symbols(const char *str, const char *symbols,
    int num_symbols)
{
    if (!str || !symbols || num_symbols <= 0)
        return 0;
    const char *s, *ls;
    ls = symbols + num_symbols;
    for (const char *c = str; *c; ++c)
        for (s = symbols; s < ls; ++s)
            if (*c == *s)
                return 1;
    return 0;
}

bool32
str_contains_one_or_more_of_symbolsn(const char *str, const char *symbols,
    int num_symbols, int str_len)
{
    if (!str || !symbols || num_symbols <= 0 || str_len <= 0)
        return 0;
    const char  *sym, *ls, *lc;
    lc = str + str_len;
    ls = symbols + num_symbols;
    for (const char *c = str; c < lc && *c; ++c)
        for (sym = symbols; sym < ls; ++sym)
            if (*c == *sym)
                return 1;
    return 0;
}

bool32
str_contains_only_symbols(const char *str, const char *symbols,
    int num_symbols)
{
    if (!str || !symbols || num_symbols <= 0)
        return 1;
    const char  *s, *ls;
    ls = symbols + num_symbols;
    for (const char *c = str; *c; ++c)
        for (s = symbols; s < ls; ++s)
            if (*c == *s)
                return 0;
    return 1;
}

bool32
str_contains_only_symbolsn(const char *str, const char *symbols,
    int num_symbols, int str_len)
{
    if (!str || !symbols || num_symbols <= 0 || str_len <= 0)
        return 1;
    const char  *sym, *lsym, *lc;
    lc      = str + str_len;
    lsym    = symbols + num_symbols;
    for (const char *c = str; c < lc && *c; ++c)
        for (sym = symbols; sym < lsym; ++sym)
            if (*c == *sym)
                return 0;
    return 1;
}

int
str_insensitive_cmp(const char *str1, const char *str2)
{
    int a = strlen(str1);
    int b = strlen(str2);
    if (a != b)
        return 1;
    int k;
    for (k = 0; k < a; ++k)
        if ((str1[k] | 32) != (str2[k] | 32))
            break;
    if (k != a)
        return 1;
    return 0;
}

void
str_purge_trailing_spaces(char *str)
{
    if (!str)
        return;
    int len, i;
    len = strlen(str);
    for (i = 0; i < len; ++i)
        if (str[i] != ' ' && str[i] != '\t')
            break;
    len = len - i;
    memmove(str, str + i, len);
    str[len] = '\0';
    for (i = len - 1; i >= 0; --i) {
        if (str[i] != ' ' && str[i] != '\t')
            break;
        str[i] = '\0';
    }
}

void
str_purge_non_numbers(char *str)
{
	static char numbers[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
	str_strip_undef_symbols(str, numbers, 10);
}

int
str_to_bool(const char *str, bool32 *ret_bool)
{
    if (!str) return 1;
    char ts[6];
    strncpy(ts, str, 6);
    str_to_lower(ts);
    int ret = 0;
    if (streq(ts, "false") || streq(ts, "0"))
        *ret_bool = 0;
    else if (streq(ts, "true") || streq(ts, "1"))
        *ret_bool = 1;
    else
        ret = 2;
    return ret;
}

void
str_to_upper(char *str)
    {for (char *c = str; *c; ++c) *c = (char)toupper(*c);}

void
str_to_lower(char *str)
    {for (char *c = str; *c; ++c) *c = (char)tolower(*c);}

char *
str_find(char *str, const char *key)
{
    char *ret = 0;
    if (!str || ! key) return ret;

    uint64 len = strlen(key);
    if (!len) return ret;

    uint64  num_correct = 0;
    char    *pot = str;

    for (char *c = str; *c; ++c) {
        if (*c != key[num_correct]) {
            num_correct = 0;
            pot = c + 1;
            continue;
        }
        if ((++num_correct) != len)
            continue;
        ret = pot;
        break;
    }
    return ret;
}

uint32
fnv_hash_from_str(const char *str)
{
    #define FNV_32_SEED 0x811C9DC5;
    #define FNV_32_PRIME ((uint32)0x01000193)
    uint32 hash = FNV_32_SEED;
    for (const char *c = str; *c; ++c)
        {hash ^= *c; hash *= FNV_32_PRIME;}
    return hash;
}

bool32
str_is_valid_file_name(const char *str)
{
    if (!str)       return 0;
    if (!str[0])    return 0;
    static char syms[] =
    {
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
        'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B',
        'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
        'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '.', '-', '_',
    };
    return str_contains_only_symbols(str, syms, sizeof(syms));
}

bool32
str_is_valid_file_path(const char *str)
{
    if (!str)       return 0;
    if (!str[0])    return 0;
    static char syms[] =
    {
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
        'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B',
        'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
        'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '.', '-', '_', '/'
    };
    return str_contains_only_symbols(str, syms, sizeof(syms));
}

typedef struct {
    uint32 len, cap; /* Neither number includes the null terminator */
} dstr_header_t;

static dchar *
_create_empty_dynamic_str_inner(uint len)
{
    uint32          cap = len + len % 4;
    dstr_header_t   *h  = malloc(sizeof(dstr_header_t) + cap + 1);
    if (!h)
        return 0;
    h->len = 0;
    h->cap = cap;
    return (dchar*)(h + 1);
}

static dstr_header_t *
_grow_dynamic_str_cap(dstr_header_t *h, uint cap)
{
    uint32 max = cap * 110 / 100;
    uint32 req = cap + cap % 4;
    if (max < req) max = req;
    dstr_header_t *nh = realloc(h, sizeof(dstr_header_t) + max + 1);
    if (nh) nh->cap = max;
    return nh;
}

dchar *
create_dynamic_str(const char *txt)
{
    if (!txt)
        return 0;
    uint32  len = strlen(txt);
    dchar *ret  = _create_empty_dynamic_str_inner(len);
    if (ret) {
        memcpy(ret, txt, len + 1);
        ((dstr_header_t*)ret - 1)->len = len;
    }
    return ret;
}

dchar *
create_empty_dynamic_str(uint len)
{
    if (!len) return 0;
    return _create_empty_dynamic_str_inner(len);
}

void
free_dynamic_str(const dchar *str)
    {if (str) free((dstr_header_t*)str - 1);}

dchar *
set_dynamic_str(dchar *str, const char *txt)
{
    if (!str) return create_dynamic_str(txt);
    if (!txt) return 0;
    uint32 len = strlen(txt);
    if (!len) return 0;

    dstr_header_t *h = (dstr_header_t*)str - 1;

    if (h->cap < len) {
        dstr_header_t *nh = _grow_dynamic_str_cap(h, len);
        if (!nh) {return 0;}
        h = nh;
    }

    h->len = len;
    char *ret = (char*)(h + 1);
    memcpy(ret, txt, len + 1);
    return ret;
}

uint
get_dynamic_str_len(const dchar *str)
    {if (str) return ((dstr_header_t*)str - 1)->len; return 0;}

uint
get_dynamic_str_cap(const dchar *str)
    {if (str) return ((dstr_header_t*)str - 1)->cap; return 0;}

dchar *
set_dynamic_str_len(dchar *str, uint len)
{
    dstr_header_t *h = (dstr_header_t*)str - 1;
    if (len > h->cap) {
        dstr_header_t *nh = _grow_dynamic_str_cap(h, len);
        if (!nh)
            return 0;
        h = nh;
    }
    h->len = len;
    ((dchar*)(h + 1))[len] = 0;
    return (dchar*)(h + 1);
}

dchar *
append_to_dynamic_str(dchar *dstr, const char *s)
{
    if (!dstr)  return create_dynamic_str(s);
    if (!s)     return dstr;

    dstr_header_t *h    = (dstr_header_t*)dstr - 1;
    uint len            = strlen(s);
    uint req_cap        = h->len + len;

    if (req_cap > h->cap) {
        h = _grow_dynamic_str_cap(h, req_cap + req_cap % 4);
        if (!h) return 0;
    }

    char *ndstr = (char*)(h + 1);
    memcpy(ndstr + h->len, s, len + 1);
    h->len += len;
    return ndstr;
}

int
append_to_dynamic_str_ptr(dchar **dstrp, const char *s)
{
    if (!dstrp)
        return 1;
    dchar *tmp = append_to_dynamic_str(*dstrp, s);
    if (!tmp)
        return 2;
    *dstrp = tmp;
    return 0;
}

int
set_dynamic_str_ptr(dchar **dstrp, const char *s)
{
    if (!dstrp)
        return 1;
    dchar *tmp = set_dynamic_str(*dstrp, s);
    if (s && s[0] && !tmp)
        return 2;
    *dstrp = tmp;
    return 0;
}

dchar *
dstr_create(const char *s)
{
    dchar *ret = create_dynamic_str(s);
    if (!ret && s)
        exit(EXIT_FAILURE);
    return ret;
}

dchar *
dstr_create_empty(uint len)
{
    dchar *ret = create_empty_dynamic_str(len);
    if (!ret && len > 0)
        exit(EXIT_FAILURE);
    return ret;
}

void
dstr_free(dchar **dstr)
    {if (dstr) {free_dynamic_str(*dstr); *dstr = 0;}}

void
dstr_set(dchar **dstr, const char *s)
{
    if (!dstr) return;
    dchar *ret = set_dynamic_str(*dstr, s);
    if (!ret && s && s[0]) exit(EXIT_FAILURE);
    *dstr = ret;
}

void
dstr_append(dchar **dstr, const char *s)
{
    if (!dstr)
        return;
    dchar *ret = append_to_dynamic_str(*dstr, s);
    if (!ret && s && s[0])
        exit(EXIT_FAILURE);
    *dstr = ret;
}

void
dstr_replace(dchar **dstr, const char *comparand, const char *replacement)
{
    if (!dstr || !(*dstr) || !comparand || !replacement)
        return;

    int     clen    = strlen(comparand);
    int     rlen    = strlen(replacement);
    int     diff    = rlen - clen;
    char    *s      = *dstr;

    while ((s = str_find(s, comparand))) {
        int req_len = (int)dstr_len(*dstr) + diff;
        if (req_len > (int)dstr_cap(*dstr)) {
            size_t pdiff = (size_t)(s - *dstr);
            *dstr = (dchar*)(_grow_dynamic_str_cap(
                ((dstr_header_t*)*dstr) - 1, req_len + 4) + 1);
            s = *dstr + pdiff;
        }
        int num = dstr_len(*dstr) - (int)(size_t)(s - *dstr) + clen;
        memmove(s + rlen, s + clen, num);
        memcpy(s, replacement, rlen);
        set_dynamic_str_len(*dstr, req_len);
        s += clen < rlen ? clen : rlen;
    }
}

void
dstr_erase_range(dchar **dstr, uint first, uint last)
{
    if (!dstr || !*dstr || first >= last || first >= dstr_len(*dstr))
        return;
    uint num_bytes = dstr_len(*dstr) - last - 1;
    memmove(*dstr + first, *dstr + last + 1, num_bytes);
    dstr_header_t *h = ((dstr_header_t*)*dstr) - 1;
    h->len -= num_bytes;
    (*dstr)[h->len] = 0;
}

#define CFG_PARSE_BUF_SZ 1024

int
parse_cfg_file(const char *path,
    void (*callback)(void *ctx, const char *opt, const char *val),
    void *context)
{
    if (!path)      return 1;
    if (!callback)  return 2;

    FILE *f = fopen(path, "r");
    if (!f) return 3;

    char    stack_buf[CFG_PARSE_BUF_SZ];
    int     ret         = 0;
    char    *buf        = stack_buf;
    char    *val        = 0;
    uint    buf_max     = CFG_PARSE_BUF_SZ;
    int     i           = 0;
    bool32  have_first  = 0;
    bool32  eof;

    for (;;) {
        buf[i]  = fgetc(f);
        eof     = buf[i] == EOF || !buf[i];

        if (buf[i] == '\n' || eof) {
            if (val) {
                buf[i]      = 0;
                char *opt   = buf;
                str_purge_trailing_spaces(opt);
                str_purge_trailing_spaces(val);
                if (opt[0] && val[0] && !streq(val, "(null)"))
                    callback(context, opt, val);
            }
            i           = 0,
            val         = 0;
            have_first  = 0;

            if (!eof) continue;
            break;
        }

        if (!val) {
            if (!have_first && !(buf[i] == ' ' || buf[i] == '\t')) {
                if (buf[i] == '#') /* Line is a comment */ {
                    int c = fgetc(f);;
                    for (; c != EOF && c != '\n' && c; c = fgetc(f));
                    i = 0;
                    continue;
                }
                have_first = 1;
            } else if (buf[i] == '=') {
                val     = buf + i + 1;
                buf[i]  = 0;
            }
        }

        if ((i++) != buf_max)
            continue;

        /* Buffer full - count the size required and attempt to heap alloc */
        long int    offset      = ftell(f);
        int         line_len    = i;
        int         c;

        while ((c = fgetc(f)) != EOF && c) line_len++;
        fseek(f, offset, SEEK_SET);

        int     buf_sz      = line_len * 110 / 100 + 1;
        size_t  val_offset  = (size_t)(val - buf);

        if (buf == stack_buf) {
            char *new_buf = malloc(buf_sz);
            if (!new_buf) {ret = 4; goto out;}
            memcpy(new_buf, buf, i);
            buf = new_buf;
        } else {
            buf = realloc(buf, buf_sz);
            if (!buf) {ret = 5; goto out;}
        }

        if (val)
            val = buf + val_offset;
        buf_max = buf_sz;
    }

    out:
        fclose(f);
        if (buf != stack_buf) free(buf);
        return ret;
}

int
append_text_file_to_another(FILE *dst, FILE *src)
{
    if (!dst) return 1;
    if (!src) return 2;
    char    buf[512];
    int     num_chars = 0;
    while (!feof(src)) {
        buf[num_chars++] = fgetc(src);
        if (num_chars == 512) {
            fwrite(buf, 1, num_chars, dst);
            num_chars = 0;
        }
    }
    fwrite(buf, 1, num_chars, dst);
    return 0;
}

char *
ascii_time()
{
    time_t raw_time;
    time(&raw_time);
    struct tm *time_info = localtime(&raw_time);
    return asctime(time_info);
}

struct tm *
get_local_time_info()
{
    time_t raw_time;
    time(&raw_time);
    return localtime(&raw_time);
}

int
week_day_from_date(int d, int m, int y)
{
    if (d > 31) return -1;
    if (d < 1)  return -2;
    if (m > 12) return -3;
    if (m < 1)  return -4;
    if (y < 1)  return -5;
    /* Literally taken from Wikipedia */
    int ret = (d += m < 3 ? y-- : y - 2, 23 * m / 9 + d + 4 + y / 4 - y / 100 \
        + y / 400) % 7;
    ret = ret == 0 ? 7 : ret;
    return ret;
}

const char *
month_to_str(uint month)
{
    switch (month)
    {
    case 1:     return "January";
    case 2:     return "February";
    case 3:     return "March";
    case 4:     return "April";
    case 5:     return "May";
    case 6:     return "June";
    case 7:     return "July";
    case 8:     return "August";
    case 9:     return "September";
    case 10:    return "October";
    case 11:    return "November";
    case 12:    return "December";
    default:    return 0;
    }
}

const char *
week_day_to_str(uint day)
{
    switch (day)
    {
    case 1: return "Monday";
    case 2: return "Tuesday";
    case 3: return "Wednesday";
    case 4: return "Thursday";
    case 5: return "Friday";
    case 6: return "Saturday";
    case 7: return "Sunday";
    default: return 0;
    }
}

int
uint_to_zero_padded_str(uint val, uint ret_buf_len, char *ret_buf)
{
    if (!ret_buf_len)   return 0;
    if (!ret_buf)       return 1;
    uint len = snprintf(ret_buf, ret_buf_len, "%u", val);
    if (len >= ret_buf_len)
        return 0;
    memmove(ret_buf + ret_buf_len - 1 - len, ret_buf, len);
    uint num_over = ret_buf_len - 1 - len;
    for (uint i = 0; i < num_over; ++i)
        ret_buf[i] = '0';
    return 0;
}
