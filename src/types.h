#ifndef TYPES_H
#define TYPES_H

#include <stdint.h>

typedef unsigned char           uchar;
typedef unsigned short          ushort;
typedef unsigned int            uint;
typedef int8_t                  int8;
typedef uint8_t                 uint8;
typedef int32_t                 int32;
typedef uint32_t                uint32;
typedef int32_t                 bool32;
typedef int8_t                  bool8;
typedef int16_t                 int16;
typedef uint16_t                uint16;
typedef int64_t                 int64;
typedef uint64_t                uint64;
typedef long int                lint;
typedef long long int           llint;
typedef long unsigned int       luint;
typedef long long unsigned int  lluint;
typedef char                    dchar; /* Dynamic string */

#endif
