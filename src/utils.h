#ifndef UTILS_H
#define UTILS_H

#include "types.h"

typedef struct darr_head_t darr_head_t;

struct darr_head_t
{
    uint32 num;
    uint32 cap;
};

#define MIN(val, min) ((val) < (min) ? (val) : (min))
/* Return the lower one of two values */

#define MAX(val, max) ((val) > (max) ? (val) : (max))
/* Return the higher one of two values */

#define CLAMP(val, min, max) (MAX((MIN((val), (max))), (min)))

#define ABS(val) ((val) < 0 ? -(val) : (val))

#define FLOORF(val) (val >= 0.0f ? (float)(int)(val) : (float)((int)(val) - 1))

#define CEILF(val) ((val) - (float)(int)(val) != 0.0f ? \
    (val) > 0.0f ? ((float)((int)(val) + 1)) : (float)(int)(val) : \
    val)

/* If remainder is exactly 0.5f, move away from zero */
#define ROUNDF(val) ((val) >= 0.0f ? \
    ((val) - (float)(int)(val) >= 0.5f ? \
        (float)((int)(val) + 1) : (float)(int)(val)) \
    : \
    ((val) + (float)(-(int)(val)) <= -0.5f ? \
         (float)((int)(val) - 1) : (float)(int)(val)))

#define GET_BITFLAG(val, flag)      (((val) & (flag)) == (flag))
#define SET_BITFLAG_ON(val, flag)   ((val) |= (flag))
#define SET_BITFLAG_OFF(val, flag)  ((val) &= ~(flag))

void *
emalloc(size_t sz);

void *
erealloc(void *mem, size_t sz);

void *
darr_ensure_growth_by(void *darr, uint32 grow_by, uint32 item_sz);

#define _darr_head(darr) ((darr_head_t*)(darr) - 1)
#define darr_head(darr) ((darr) ? _darr_head(darr) : 0)
#define darr_num(darr)  ((darr) ? ((darr_head_t*)(darr) - 1)->num : 0)
#define darr_cap(darr)  ((darr) ? ((darr_head_t*)(darr) - 1)->cap : 0)
#define darr_reserve(darr, cap_) \
    ((darr) = darr_ensure_growth_by((darr), \
        darr_num(darr) < (cap_) ? (cap_) - darr_num(darr) : 0, \
        sizeof(*(darr))))
#define darr_free(darr) (free(darr_head(darr)), darr = 0)
#define darr_push(darr, val) \
    ((darr) = darr_ensure_growth_by((darr), 1, sizeof(*(darr))), \
    (darr)[darr_head(darr)->num++] = (val))
#define darr_push_empty(darr) \
    ((darr) = darr_ensure_growth_by((darr), 1, sizeof(*(darr))), \
    &(darr)[darr_head(darr)->num++])
#define darr_clear(darr) \
    (darr_head(darr) ? darr_head(darr)->num = 0 : 0)
#define darr_erase_last(darr) \
    ((darr) ? (_darr_head(darr)->num ? _darr_head(darr)->num-- : 0) : 0)
#define darr_erase(darr, index) \
    (darr_sized_erase(darr, index, sizeof(*(darr))))

char *
load_file_to_str(const char *fp);
/* Loads in binary format. Must be freed using free() */

char *
load_text_file_to_dynamic_str(const char *fp);

void
str_strip_symbols(char *str, const char *symbols, int num_symbols);

int
str_strip_symbolsn(char *str, const char *symbols, int num_symbols,
    int str_len);
/* Returns the new length of the string */

void
str_strip_undef_symbols(char *str, const char *symbols, int num_symbols);
/* Strip out symbols NOT defined in the 'symbols' parameter */

void
str_strip_non_alpha(char *str);

bool32
str_contains_one_or_more_of_symbols(const char *str, const char *symbols,
    int num_symbols);

bool32
str_contains_one_or_more_of_symbolsn(const char *str, const char *symbols,
    int num_symbols, int str_len);

bool32
str_contains_only_symbols(const char *str, const char *symbols,
    int num_symbols);

bool32
str_contains_only_symbolsn(const char *str, const char *symbols,
    int num_symbols, int str_len);

int
str_insensitive_cmp(const char *str1, const char *str2);

void
str_purge_trailing_spaces(char *str);
/* Remove spaces at the beginning of a string and pad with zeros at the end.
 * Also remvoves tabs */

bool32
str_is_valid_file_name(const char *str);

bool32
str_is_valid_file_path(const char *str);

void
str_purge_non_numbers(char *str);

int
str_to_bool(const char *str, bool32 *ret_bool);

void
str_to_upper(char *str);

void
str_to_lower(char *str);

char *
str_find(char *str, const char *key);

#define str_to_uint64(str) (strtoul((str), 0, 10))
#define str_to_uint32(str) ((uint32)str_to_uint64(str))
#define streq(sa, sb) (strcmp((sa), (sb)) == 0)

uint32
fnv_hash_from_str(const char *str);

dchar *
create_dynamic_str(const char *txt);

dchar *
create_empty_dynamic_str(uint len);

void
free_dynamic_str(const dchar *str);

dchar *
set_dynamic_str(dchar *str, const char *txt);

uint
get_dynamic_str_len(const dchar *str);

uint
get_dynamic_str_cap(const dchar *str);

dchar *
set_dynamic_str_len(dchar *str, uint len);

dchar *
append_to_dynamic_str(dchar *dstr, const char *s);

dchar *
insert_to_dynamic_str(dchar *dstr, const char *s, uint index);

dchar *
erase_range_from_dynamic_str(dchar *dstr, uint begin, uint end);

int
append_to_dynamic_str_ptr(dchar **dstrp, const char *s);

int
set_dynamic_str_ptr(dchar **dstrp, const char *s);

dchar   *dstr_create(const char *s);
dchar   *dstr_create_empty(uint len);
void    dstr_free(dchar **dstr);
void    dstr_set(dchar **dstr, const char *s);
void    dstr_append(dchar **dstr, const char *s);
void dstr_replace(dchar **dstr, const char *comparand, const char *replacement);
void    dstr_erase_range(dchar **dstr, uint first, uint last);
#define dstr_len(dstr) (get_dynamic_str_len(dstr))
#define dstr_cap(dstr) (get_dynamic_str_cap(dstr))

int
append_text_file_to_another(FILE *dst, FILE *src);

char *
ascii_time();

struct tm *
get_local_time_info();

int
week_day_from_date(int d, int m, int y);
/* Returns < 0 on error */

const char *
month_to_str(uint month);

const char *
week_day_to_str(uint day);

int
uint_to_zero_padded_str(uint val, uint ret_buf_len, char *ret_str);
/* Returns success even if resulting str is longer than ret_buf_len - 1 */

#endif
