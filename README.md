# jeesgen
A static website generator in C. Written strictly for my own use. You know, for
websites and stuff.

## Building
### GNU/Linux
```
$ ./configure
$ make
```

### Windows
```
$ configure.bat
$ nmake
```

## Usage

### Example
```
$ ./jeesgen --new my_project
$ printf "title = my first post\n" > post.txt
$ printf "author = myself\n" >> post.txt
$ echo "hello world!" >> post.txt
$ ./jeesgen --post my_project post.txt
$ printf "name = my page\n" > page.txt
$ echo "hello world!" >> page.txt
$ ./jeesgen --page my_project page.txt
$ ./jeesgen --generate my_project
```

The generated HTML is placed in the "generated" subdirectory of the project by
default.

### Configuration
'jees_config.cfg' and the template .html files inside a new project
directory may be configured to get the end result look as wanted.  HTML tags
beginning with "jees-" are used to mark spots of content injection in the
layout files.
