#!/bin/bash
rm -rf jeesgen_linux64
./configure
make
mkdir jeesgen_linux64
cp build/jeesgen jeesgen_linux64/jeesgen
cp COPYING jeesgen_linux64/COPYING
mkdir jeesgen_linux64/code
cp -r src jeesgen_linux64/code/src
cp configure jeesgen_linux64/code/configure
cp README.md jeesgen_linux64/code/README.md
find jeesgen_linux64 -type f -name '*~' -exec rm {} +
find jeesgen_linux64 -type f -name '*.swp' -exec rm {} +
find jeesgen_linux64 -type f -name '*.swpo' -exec rm {} +
